
let print_pos lb =
    let start = lb.Lexing.lex_start_p in
    let file = start.Lexing.pos_fname in
    let line = start.Lexing.pos_lnum  in
    let cnum = start.Lexing.pos_cnum - start.Lexing.pos_bol in
    let tok = Lexing.lexeme lb in
    Format.eprintf "File: %s, line: %d, column: %d, Token \"%s\"@\n@."
      file line cnum tok

let parse lb =
  try
    Parser.line Lexer.token lb
  with
  | e -> print_pos lb; raise e
