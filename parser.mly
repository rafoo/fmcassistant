%{


%}

%token F R U B L D FW RW UW BW LW DW SQ PRIME X Y Z M E S EOF DISPLAY LET DEF IN
%token <string> CUSTOM
%type <Move.cmd list> line
%type <Move.move> move
%type <int> repeat
%start line
%%

command: move { Move.Mv ($1, 1) }
       | move repeat { Move.Mv ($1, $2) }
       | DISPLAY { Move.Disp }
       | CUSTOM { Move.Custom ($1, false) }
       | CUSTOM PRIME { Move.Custom ($1, true) }

moves: IN { [] }
       | move moves { Move.Mv ($1, 1) :: $2 }
       | move repeat moves { Move.Mv ($1, $2) :: $3 }

line: EOF { [] }
      | command line { $1 :: $2 }
      | LET CUSTOM DEF moves line { Hashtbl.add Move.defs $2 $4; $5 }
;

move: F { Move.F }
    | R { Move.R }
    | U { Move.U }
    | B { Move.B }
    | L { Move.L }
    | D { Move.D }
    | FW { Move.Fw }
    | RW { Move.Rw }
    | UW { Move.Uw }
    | BW { Move.Bw }
    | LW { Move.Lw }
    | DW { Move.Dw }
    | X { Move.X }
    | Y { Move.Y }
    | Z { Move.Z }
    | M { Move.M }
    | E { Move.E }
    | S { Move.S }
;

repeat: SQ { 2 }
      | PRIME { 3 }
;

%%