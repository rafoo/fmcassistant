
val all_distinct : 'a list -> bool
val last : 'a list -> 'a
val butlast : 'a list -> 'a list
val ( @^ ) : 'a list -> int -> 'a list
