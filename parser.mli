type token =
  | F
  | R
  | U
  | B
  | L
  | D
  | FW
  | RW
  | UW
  | BW
  | LW
  | DW
  | SQ
  | PRIME
  | X
  | Y
  | Z
  | M
  | E
  | S
  | EOF
  | DISPLAY
  | LET
  | DEF
  | IN
  | CUSTOM of (string)

val line :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> Move.cmd list
