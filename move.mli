type move =
  (* Regular moves *)
  | F | R | U | B | L | D
  (* Wide moves using two layers *)
  | Fw | Rw | Uw | Bw | Lw | Dw
  (* Whole cube rotations *)
  | X | Y | Z
  (* Slice moves *)
  | M | E | S

type cmd = Mv of (move * int) | Disp | Custom of string * bool

val defs : (string, cmd list) Hashtbl.t

val seq_to_string : cmd list -> string

val unfold : cmd list -> cmd list
val normalize : cmd list -> cmd list

val inverse : cmd -> cmd
val inverse_seq : cmd list -> cmd list
