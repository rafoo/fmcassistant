open Face

type t = UF | UR | UB | UL | DF | DR | DB | DL | FR | BR | FL | BL
let n = 12
let orientations = 2
let arr = [| UF; UR; UB; UL; DF; DR; DB; DL; FR; BR; FL; BL |]
let of_int n = arr.(n)

let to_int = function
  | UF -> 0 | UR -> 1 | UB -> 2 | UL -> 3
  | DF -> 4 | DR -> 5 | DB -> 6 | DL -> 7
  | FR -> 8 | BR -> 9 | FL -> 10 | BL -> 11

type cube = (t * int) array

(* Gives the positions of the two stickers of an edge *)
let edge_pos e o =
  let arr = [|
   [| (U, 1, 0); (F, 1, 2) |] ; [| (U, 2, 1); (R, 1, 2) |] ; [| (U, 1, 2); (B, 1, 2) |] ; [| (U, 0, 1); (L, 1, 2) |] ;
   [| (D, 1, 2); (F, 1, 0) |] ; [| (D, 2, 1); (R, 1, 0) |] ; [| (D, 1, 0); (B, 1, 0) |] ; [| (D, 0, 1); (L, 1, 0) |] ;
   [| (F, 2, 1); (R, 0, 1) |] ; [| (B, 0, 1); (R, 2, 1) |] ; [| (F, 0, 1); (L, 2, 1) |] ; [| (B, 2, 1); (L, 0, 1) |] |] in
  arr.(to_int e).(o mod 2)

let oriented_edge_from_faces f1 f2 =
  match (f1, f2) with
  | (U, F) -> (UF, 0) | (F, U) -> (UF, 1)
  | (U, R) -> (UR, 0) | (R, U) -> (UR, 1)
  | (U, B) -> (UB, 0) | (B, U) -> (UB, 1)
  | (U, L) -> (UL, 0) | (L, U) -> (UL, 1)
  | (D, F) -> (DF, 0) | (F, D) -> (DF, 1)
  | (D, R) -> (DR, 0) | (R, D) -> (DR, 1)
  | (D, B) -> (DB, 0) | (B, D) -> (DB, 1)
  | (D, L) -> (DL, 0) | (L, D) -> (DL, 1)
  | (F, R) -> (FR, 0) | (R, F) -> (FR, 1)
  | (B, R) -> (BR, 0) | (R, B) -> (BR, 1)
  | (F, L) -> (FL, 0) | (L, F) -> (FL, 1)
  | (B, L) -> (BL, 0) | (L, B) -> (BL, 1)
  | _ -> failwith "oriented_edge_from_faces"

let get_edge cube e =
  let (f1, x1, y1) = edge_pos e 0 in
  let (f2, x2, y2) = edge_pos e 1 in
  let col1 = Facelet.get_face cube f1 x1 y1 in
  let col2 = Facelet.get_face cube f2 x2 y2 in
  oriented_edge_from_faces col1 col2

let of_facelet cube = Array.map (get_edge cube) arr

let to_string (e, o) =
  let ((f1, _, _), (f2, _, _)) = (edge_pos e o, edge_pos e (1 - o)) in
  Face.to_string f1 ^ Face.to_string f2

let cube_to_string cube =
  let a = ref "[" in
  Array.iteri
    (fun i e ->
      a := !a ^
        Printf.sprintf "%s -> %s, "
           (to_string (arr.(i), 0))
           (to_string e))
    cube;
  !a ^ "]"
