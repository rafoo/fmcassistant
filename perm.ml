module type FINITE =
  sig
    type t                      (* Some finite type *)
    val n : int                 (* The cardinal *)
    val all : t list            (* The list of all elements *)
  end

module type MODULOINT =
  sig
    type t
    val n : int
    val all : t list

    val z : t
    val (+) : t -> t -> t
  end


module type ORIENTABLE =
  sig
    type t                      (* The type of the orientable pieces *)
    type o                      (* The type of the orientations *)
    type oriented               (* oriented ~ t * o *)
    val of_int : int -> t
    val to_int : t -> int
    val of_int : int -> t
    val to_int : t -> int
    val to_oriented : (t * o) -> oriented
    val of_oriented : oriented -> (t * o)
    val same_fst : oriented -> oriented -> bool
    val n : int                 (* The number of pieces on the cube *)
    val orientations : int      (* The number of possible orientations of each piece *)

    val get : Facelet.cube -> oriented -> oriented
    val to_string : oriented -> string
  end

module type PERM =
  sig
    type c
    type t
    val of_array : c array -> t
    val to_array : t -> c array
    val to_string : t -> string
  end

module type PUREPERM =
  sig
    type c
    type fullperm
    type t
    val of_array : c array -> t
    val to_array : t -> c array
    val of_fullperm : fullperm -> t
    val to_int : t -> int
    val of_int : int -> t
  end

module type PUREORIENT =
  sig
    type c
    type fullperm
    type t
    val of_array : int array -> t
    val to_array : t -> int array
    val of_fullperm : fullperm -> t
    val to_int : t -> int
    val of_int : int -> t
  end

module type FULLCYCLE =
  sig
    type c
    type fullperm
    type t
    val to_list : t -> (c * int) list
    val from_list : (c * int) list -> t
    val apply_cycle : t -> (c * int) -> (c * int)
    val perm_to_cycles : cperm -> t array
    val to_string : t -> string
    val cycles_to_string : t array -> string
  end

module type PURECYCLE =
  sig
    type c
    type cperm
    type t
    val to_list : t -> (c * int) list
    val from_list : (c * int) list -> t
    val apply_cycle : t -> (c * int) -> (c * int)
    val perm_to_cycles : cperm -> t array
    val to_string : t -> string
    val cycles_to_string : t array -> string
  end


exception Bad_cycle

module Cycle (C : ORIENTABLE) : CYCLE with type c = C.t and type cperm = C.cube =
  struct

    (* A good cycle is a list of length >= 2 such that:
       - the first and the last element are identical (up to orientation)
       - apart from that, all elements are distinct

      Examples:
       [ (a, _); (a, _) ]
       [ (a, _); (b, _); (a, _) ]
       [ (a, _); (b, _); (c, _); (d, _); (a, _) ] *)
    type t = (C.t * int) list
    type c = C.t
    type cperm = C.cube

    let to_list l = l
    let from_list l =
      assert (List.length l >= 2);
      assert (Mylist.all_distinct (List.tl l));
      assert (fst (List.hd l) = fst (Mylist.last l));
      l

    let rec apply_cycle cycle (c, o) = match cycle with
      | [] -> raise Bad_cycle
      | [ (c', o') ] ->
         if c = c' then raise Bad_cycle else (c, o)
      | (c1, o1) :: (c2, o2) :: l ->
         if c = c1 then
           (c2, (C.orientations + o + o2 - o1) mod C.orientations)
         else apply_cycle ((c2, o2) :: l) (c, o)

    let perm_to_cycles cube =
      let res = Array.make C.n [] in
      for i = 0 to C.n - 1 do
        let current = C.of_int i in
        if res.(i) = [] then
          begin
            let o = ref 0 in
            let c = ref i in
            let stack = ref [(current, 0)] in
            while fst(cube.(!c)) <> current do
              o := (!o + snd(cube.(!c))) mod C.orientations;
              let cuby = fst(cube.(!c)) in
              c := C.to_int cuby;
              stack := (cuby, !o) :: !stack
            done;
            o := (!o + snd(cube.(!c))) mod C.orientations;
            stack := (current, !o) :: !stack;
            let cycle = from_list (List.rev !stack) in
            List.iter (fun (c, _) -> res.(C.to_int c) <- cycle) !stack
          end;
      done;
      res

    let to_string c = "(" ^ String.concat " " (List.map C.to_string c) ^ ")"

    let cycles_to_string cycles =
      let buff = ref "" in
      Array.iteri
        (fun i c ->
          match c with
          | [_; (_, 0)] -> ()    (* Pure loops are not displayed *)
          | _ ->
             if C.to_int (fst(List.hd c)) = i then buff := !buff ^ " " ^ to_string c)
        cycles;
      !buff
  end
