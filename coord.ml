(* At coordinate level, the cube is represented by a tuple of natural numbers *)

(* These functions are taken from http://kociemba.org/math/coordlevel.htm *)

(* n: number of cubies of this kind
   o: number of possible orientations for each cuby

   orientation of the last cuby is ignored as it can be computed from
   the others *)
let orient_coord orients n o =
  let res = ref 0 in
  for i = 0 to n - 2 do
    res := (o * !res) + orients.(i)
  done;
  !res

let rev_orient_coord coord n o =
  let res = Array.make n 0 in
  let rem = ref 0 in
  let k = ref coord in
  for i = n-2 downto 0 do
    let a = !k mod o in
    rem := (!rem + a) mod o;
    k := !k / o;
    res.(i) <- a;
  done;
  res.(n-1) <- ((o - !rem) mod o);
  res

let perm_coord perm n =
  let res = ref 0 in
  for i = n-1 downto 1 do
    let s = ref 0 in
    for j = i-1 downto 0 do
      if perm.(j) > perm.(i) then s := !s + 1
    done;
    res := i * (!res + !s)
  done;
  !res

let rev_perm_coord coord n =
  let res = Array.init n (fun i -> i) in
  for i = 1 to n-1 do
    let s = 

(* Used for separating unsolved pieces in two groups.
   This is Kociemba's slice perm
 *)
let partial_perm_coord perm size_group_2 =
  let size = Array.length perm in
  let in_group_2 c = c + size_group_2 >= size in
  let occupied = Array.init size (fun i -> in_group_2 perm.(i)) in
  let s = ref 0 in
  let k = ref (size_group_2 - 1) in
  let n = ref (size - 1) in
  while !k >= 0 do
    if occupied.(!n) then
      k := !k - 1
    else
      s := !s + choose !n !k;
    n := !n - 1;
  done;
  !s

let corner_orient_coord orients = orient_coord orients 8 3
let edge_orient_coord orients = orient_coord orients 12 2
let slice_coord perm = partial_perm coord perm 4

let corner_perm_coord perm = perm_coord perm 8
let edge_perm_coord perm = perm_coord perm 8
let slice_perm_coord perm = perm_coord (Array.iter 4 (fun i -> perm.(i+8)-8)) 4

(* 21 moves : F R U B L D F2 R2 U2 B2 L2 D2 F' R' U' B' L' D' x y z *)
let co_move = Array.init 2048 (fun _ -> Array.create 21 (-1))
