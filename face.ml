type face = U | D | F | B | R | L

let of_int n = [| U; D; F; B; R; L |].(n)

let to_int = function
  | U -> 0
  | D -> 1
  | F -> 2
  | B -> 3
  | R -> 4
  | L -> 5

let to_string f = [| "U"; "D"; "F"; "B"; "R"; "L"|].(to_int f)
