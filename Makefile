OCAMLC=ocamlc

all: fmcassistant

%.cmi: %.mli
	$(OCAMLC) $<

%.cmo: %.ml %.cmi
	$(OCAMLC) -c $<

parser.ml: parser.mly parser.cmi
	ocamlyacc $<

lexer.ml: lexer.mll
	ocamllex $<

display.cmo: display.cmi
	ocamlfind ocamlc -c -g -package lablgtk2 -linkpkg display.ml

perm.cmo: mylist.cmo
corner.cmo: perm.cmo face.cmo facelet.cmo
edge.cmo: perm.cmo face.cmo facelet.cmo
parse_sequence.cmo: parser.cmo lexer.cmo move.cmo
display.cmo: mylist.cmo move.cmo facelet.cmo parse_sequence.cmo corner.cmo edge.cmo perm.cmo
main.cmo: parser.cmo lexer.cmo corner.cmo edge.cmo display.cmo parse_sequence.cmo
facelet.cmi: face.cmi move.cmi
facelet.cmo: face.cmo move.cmo
face.cmo:
parser.cmi: move.cmi
parser.cmo: move.cmo
lexer.cmo: parser.cmo

CMOS = mylist.cmo face.cmo move.cmo parser.cmo lexer.cmo facelet.cmo parse_sequence.cmo corner.cmo edge.cmo perm.cmo display.cmo main.cmo

fmcassistant: $(CMOS)
	ocamlfind ocamlc -g -package lablgtk2 -linkpkg -o $@ $(CMOS)

clean:
	rm -f *.cm* a.out parser.ml lexer.ml
