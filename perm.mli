module type ORIENTABLE =
  sig
    type t
    val of_int : int -> t
    val to_int : t -> int
    type cube = (t * int) array
    val to_string : t * int -> string
    val cube_to_string : cube -> string
    val n : int
    val orientations : int
  end

module type CYCLE =
  sig
    type c
    type cperm
    type t
    val to_list : t -> (c * int) list
    val from_list : (c * int) list -> t
    val apply_cycle : t -> c * int -> c * int
    val perm_to_cycles : cperm -> t array
    val to_string : t -> string
    val cycles_to_string : t array -> string
  end

exception Bad_cycle

module Cycle : functor (C : ORIENTABLE) ->
   CYCLE with type c = C.t and type cperm = C.cube
