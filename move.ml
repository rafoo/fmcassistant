type move =
  (* Regular moves *)
  | F | R | U | B | L | D
  (* Wide moves using two layers *)
  | Fw | Rw | Uw | Bw | Lw | Dw
  (* Whole cube rotations *)
  | X | Y | Z
  (* Slice moves *)
  | M | E | S

type cmd = Mv of (move * int) | Disp | Custom of string * bool

let to_string = function
  | F -> "F" | R -> "R" | U -> "U" | B -> "B" | L -> "L" | D -> "D"
  | Fw -> "f" | Rw -> "r" | Uw -> "u" | Bw -> "b" | Lw -> "l" | Dw -> "d"
  | X -> "x" | Y -> "y" | Z -> "z" | M -> "M" | E -> "E" | S -> "S"

let cmd_to_string = function
  | Mv (mv, r) ->
     to_string mv ^
       (match r with 1 -> "" | 2 -> "2" | 3 -> "\'" | _ -> failwith "cmd_to_string")
  | Disp -> "."
  | Custom (s, b) -> "<" ^ s ^ ">" ^ (if b then "'" else "")

let seq_to_string s = String.concat " " (List.map cmd_to_string s)

let axis = function
  | F | B | Fw | Bw | S | Z -> 0
  | L | R | Lw | Rw | M | X -> 1
  | U | D | Uw | Dw | E | Y -> 2

let parallel a b = axis a = axis b

let defs : (string, cmd list) Hashtbl.t = Hashtbl.create 0

let inverse = function
  | Mv (x, i) -> Mv (x, 4 - i)
  | Custom (x, b) -> Custom (x, not b)
  | Disp -> Disp

let inverse_seq = List.rev_map inverse

let rec unfold = function
  | [] -> []
  | Mv (M, i) :: s ->
     Mv (L, 4 - i) :: Mv (R, i) :: Mv (X, 4 - i) :: unfold s
  | Mv (E, i) :: s ->
     Mv (U, i) :: Mv (D, 4 - i) :: Mv (Y, 4 - i) :: unfold s
  | Mv (S, i) :: s ->
     Mv (F, 4 - i) :: Mv (B, i) :: Mv (Z, i) :: unfold s

  | Mv (Lw, i) :: s ->
     Mv (R, i) :: Mv (X, 4 - i) :: unfold s
  | Mv (Rw, i) :: s ->
     Mv (L, i) :: Mv (X, i) :: unfold s
  | Mv (Dw, i) :: s ->
     Mv (U, i) :: Mv (Y, 4 - i) :: unfold s
  | Mv (Uw, i) :: s ->
     Mv (D, i) :: Mv (Y, i) :: unfold s
  | Mv (Bw, i) :: s ->
     Mv (F, i) :: Mv (Z, 4 - i) :: unfold s
  | Mv (Fw, i) :: s ->
     Mv (B, i) :: Mv (Z, i) :: unfold s
  | Custom (x, b) :: s ->
     begin
       try
         let xs = Hashtbl.find defs x in
         let ys = if b then inverse_seq xs else xs in
         unfold (ys @ s)
       with
       | Not_found -> failwith ("unfold " ^ x)
     end
  | m :: s -> m :: unfold s

let rotate_x_mv = function
  | U -> F
  | F -> D
  | D -> B
  | B -> U
  | R -> R
  | L -> L
  | X -> X
  | Y -> Z
  | _ -> failwith "rotate_x"

let rotate_x_cmd = function
  | Mv(Z, i) -> Mv(Y, 4 - i)
  | Mv(a, i) -> Mv(rotate_x_mv a, i)
  | Disp -> Disp
  | Custom _ -> assert false

let rec rotate_x = function
  | [] -> [Mv(X, 1)]
  | [Mv(X, 1)] -> [Mv(X, 2)]
  | [Mv(X, 2)] -> [Mv(X, 3)]
  | [Mv(X, 3)] -> []
  | a :: s ->
     rotate_x_cmd a :: rotate_x s

let rotate_x2 m = rotate_x (rotate_x m)
let rotate_x3 m = rotate_x (rotate_x2 m)

let rotate_y_mv = function
  | F -> R
  | R -> B
  | B -> L
  | L -> F
  | U -> U
  | D -> D
  | Y -> Y
  | X -> Z
  | m -> failwith ("rotate_y " ^ to_string m)

let rotate_y_cmd = function
  | Mv(Z, i) -> Mv(X, 4 - i)
  | Mv(a, i) -> Mv(rotate_y_mv a, i)
  | Disp -> Disp
  | Custom _ -> assert false

let rec rotate_y = function
  | [] -> [Mv(Y, 1)]
  | [Mv(Y, 1)] -> [Mv(Y, 2)]
  | [Mv(Y, 2)] -> [Mv(Y, 3)]
  | [Mv(Y, 3)] -> []
  | a :: s ->
     rotate_y_cmd a :: rotate_y s

let rotate_y2 m = rotate_y (rotate_y m)
let rotate_y3 m = rotate_y (rotate_y2 m)

let rotate_z_mv = function
  | R -> U
  | U -> L
  | L -> D
  | D -> R
  | F -> F
  | B -> B
  | Z -> Z
  | X -> Y
  | _ -> failwith "rotate_z"

let rotate_z_cmd = function
  | Mv(Y, i) -> Mv(X, 4 - i)
  | Mv(a, i) -> Mv(rotate_z_mv a, i)
  | Disp -> Disp
  | Custom _ -> assert false

let rec rotate_z = function
  | [] -> [Mv(Z, 1)]
  | [Mv(Z, 1)] -> [Mv(Z, 2)]
  | [Mv(Z, 2)] -> [Mv(Z, 3)]
  | [Mv(Z, 3)] -> []
  | a :: s ->
     rotate_z_cmd a :: rotate_z s

let rotate_z2 m = rotate_z (rotate_z m)
let rotate_z3 m = rotate_z (rotate_z2 m)

let rec rotate = function
  | [] -> []

  | Mv (X, 1) :: s -> rotate_x (rotate s)
  | Mv (X, 2) :: s -> rotate_x2 (rotate s)
  | Mv (X, 3) :: s -> rotate_x3 (rotate s)

  | Mv (Y, 1) :: s -> rotate_y (rotate s)
  | Mv (Y, 2) :: s -> rotate_y2 (rotate s)
  | Mv (Y, 3) :: s -> rotate_y3 (rotate s)

  | Mv (Z, 1) :: s -> rotate_z (rotate s)
  | Mv (Z, 2) :: s -> rotate_z2 (rotate s)
  | Mv (Z, 3) :: s -> rotate_z3 (rotate s)

  | mv :: s -> mv :: rotate s

let rec simpl = function
  | [] -> []
  | a :: s ->
     begin
       match a :: simpl s with
       | Mv (_, 0) :: s -> simpl s
       | Mv (x, i) :: Mv (y, j) :: s when x = y ->
          simpl (Mv (x, (i+j) mod 4) :: s)
       | Mv (x, i) :: Mv (y, j) :: s when parallel x y && y < x ->
          simpl (Mv (y, j) :: Mv (x, i) :: s)
       | s -> s
     end

let normalize s = simpl (rotate (unfold s))
