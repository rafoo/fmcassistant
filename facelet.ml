(* Each face is given by a 3x3 square.

   For F, R, B, and L, the orientation is given by putting U above.

   For U, the F face is below. For D, the F face is above. *)

(*
    |---|
    | U |
|---|---|---|---|
| L | F | R | B |
|---|---|---|---|
    | D |
    |---|
 *)

(* A facelet face is a 3x3 square represented as an array of arrays.
   the facelet (x, y) is f.(x).(y) where 0 <= x, y <= 2.  The content
   of f.(x).(y) is an integer representing the color of the facelet,
   it is a priori not restricted in the range [0..5] (so that facelets
   can be marked when looking for cycle insertions for
   example). However, the function get_face does require that. *)

(* A facelet cube is an array of faces. The correspondance between
   faces and indexes [0..5] is given by Face.of_int and Face.to_int *)

type cube = int array array array

let init_cube () =
  Array.init
    6
    (fun face ->
      Array.init
        3
        (fun _ ->
          Array.init
            3
            (fun _ -> face)))

let reinit c =
  for f = 0 to 5 do
    for x = 0 to 2 do
      for y = 0 to 2 do
        c.(f).(x).(y) <- f
      done
    done
  done

(* Rotating a face clockwise *)

let cw f =
  (* Corners *)
  let a = f.(0).(0) in
  f.(0).(0) <- f.(2).(0);
  f.(2).(0) <- f.(2).(2);
  f.(2).(2) <- f.(0).(2);
  f.(0).(2) <- a;

  (* Edges *)
  let b = f.(1).(0) in
  f.(1).(0) <- f.(2).(1);
  f.(2).(1) <- f.(1).(2);
  f.(1).(2) <- f.(0).(1);
  f.(0).(1) <- b

(* Rotating a face counter-clockwise *)
let ccw f =
  (* Corners *)
  let a = f.(0).(0) in
  f.(0).(0) <- f.(0).(2);
  f.(0).(2) <- f.(2).(2);
  f.(2).(2) <- f.(2).(0);
  f.(2).(0) <- a;

  (* Edges *)
  let b = f.(1).(0) in
  f.(1).(0) <- f.(0).(1);
  f.(0).(1) <- f.(1).(2);
  f.(1).(2) <- f.(2).(1);
  f.(2).(1) <- b

let get cube face x y = cube.(Face.to_int face).(x).(y)
let set cube face x y v = cube.(Face.to_int face).(x).(y) <- v
let get_face cube face x y =
  let i = get cube face x y in
  let res = ref (-1) in
  for f = 0 to 5 do
    if cube.(f).(1).(1) = i then res := f
  done;
  if !res = -1 then failwith "get_face" else Face.of_int !res

let u cube =
  cw (cube.(Face.to_int Face.U));
  for i = 0 to 2 do
    let a = get cube Face.F i 2 in
    set cube Face.F i 2 (get cube Face.R i 2);
    set cube Face.R i 2 (get cube Face.B i 2);
    set cube Face.B i 2 (get cube Face.L i 2);
    set cube Face.L i 2 a;
  done

let u2 cube = u cube; u cube
let u3 cube = u cube; u cube; u cube

let d3 cube =
  ccw (cube.(Face.to_int Face.D));
  for i = 0 to 2 do
    let a = get cube Face.F i 0 in
    set cube Face.F i 0 (get cube Face.R i 0);
    set cube Face.R i 0 (get cube Face.B i 0);
    set cube Face.B i 0 (get cube Face.L i 0);
    set cube Face.L i 0 a;
  done

let d2 cube = d3 cube; d3 cube
let d cube = d3 cube; d3 cube; d3 cube

let e3 cube =
  for i = 0 to 2 do
    let a = get cube Face.F i 1 in
    set cube Face.F i 1 (get cube Face.R i 1);
    set cube Face.R i 1 (get cube Face.B i 1);
    set cube Face.B i 1 (get cube Face.L i 1);
    set cube Face.L i 1 a;
  done

let e2 cube = e3 cube; e3 cube
let e cube = e3 cube; e3 cube; e3 cube

let y cube = u cube; e3 cube; d3 cube
let y2 cube = y cube; y cube
let y3 cube = y cube; y cube; y cube

let r cube =
  cw (cube.(Face.to_int Face.R));
  for i = 0 to 2 do
    let j = 2 - i in
    let a = get cube Face.F 2 i in
    set cube Face.F 2 i (get cube Face.D 2 i);
    set cube Face.D 2 i (get cube Face.B 0 j);
    set cube Face.B 0 j (get cube Face.U 2 i);
    set cube Face.U 2 i a;
  done

let r2 cube = r cube; r cube
let r3 cube = r cube; r cube; r cube

let l3 cube =
  ccw (cube.(Face.to_int Face.L));
  for i = 0 to 2 do
    let j = 2 - i in
    let a = get cube Face.F 0 i in
    set cube Face.F 0 i (get cube Face.D 0 i);
    set cube Face.D 0 i (get cube Face.B 2 j);
    set cube Face.B 2 j (get cube Face.U 0 i);
    set cube Face.U 0 i a;
  done

let l2 cube = l3 cube; l3 cube
let l cube = l3 cube; l3 cube; l3 cube

let m3 cube =
  for i = 0 to 2 do
    let j = 2 - i in
    let a = get cube Face.F 1 i in
    set cube Face.F 1 i (get cube Face.D 1 i);
    set cube Face.D 1 i (get cube Face.B 1 j);
    set cube Face.B 1 j (get cube Face.U 1 i);
    set cube Face.U 1 i a;
  done

let m2 cube = m3 cube; m3 cube
let m cube = m3 cube; m3 cube; m3 cube

let x cube = r cube; l3 cube; m3 cube
let x2 cube = x cube; x cube
let x3 cube = x cube; x cube; x cube


let f cube = x cube; u cube; x3 cube
let f2 cube = x cube; u2 cube; x3 cube
let f3 cube = x cube; u3 cube; x3 cube

let b cube = x3 cube; u cube; x cube
let b2 cube = x3 cube; u2 cube; x cube
let b3 cube = x3 cube; u3 cube; x cube

let s cube = y cube; m cube; y3 cube
let s2 cube = y cube; m2 cube; y3 cube
let s3 cube = y cube; m3 cube; y3 cube

let z cube = y cube; x3 cube; y3 cube
let z2 cube = y cube; x2 cube; y3 cube
let z3 cube = y cube; x cube; y3 cube

let fw cube = f cube; s cube
let fw2 cube = f2 cube; s2 cube
let fw3 cube = f3 cube; s3 cube

let rw cube = r cube; m3 cube
let rw2 cube = r2 cube; m2 cube
let rw3 cube = r3 cube; m cube

let uw cube = u cube; e3 cube
let uw2 cube = u2 cube; e2 cube
let uw3 cube = u3 cube; e cube

let bw cube = b cube; s3 cube
let bw2 cube = b2 cube; s2 cube
let bw3 cube = b3 cube; s cube

let lw cube = l cube; m cube
let lw2 cube = l2 cube; m2 cube
let lw3 cube = l3 cube; m3 cube

let dw cube = d cube; e cube
let dw2 cube = d2 cube; e2 cube
let dw3 cube = d3 cube; e3 cube

let apply_move cube = function
  | Move.F, 1 -> f cube
  | Move.F, 2 -> f2 cube
  | Move.F, 3 -> f3 cube
  | Move.R, 1 -> r cube
  | Move.R, 2 -> r2 cube
  | Move.R, 3 -> r3 cube
  | Move.U, 1 -> u cube
  | Move.U, 2 -> u2 cube
  | Move.U, 3 -> u3 cube
  | Move.B, 1 -> b cube
  | Move.B, 2 -> b2 cube
  | Move.B, 3 -> b3 cube
  | Move.L, 1 -> l cube
  | Move.L, 2 -> l2 cube
  | Move.L, 3 -> l3 cube
  | Move.D, 1 -> d cube
  | Move.D, 2 -> d2 cube
  | Move.D, 3 -> d3 cube
  | Move.Fw, 1 -> fw cube
  | Move.Fw, 2 -> fw2 cube
  | Move.Fw, 3 -> fw3 cube
  | Move.Rw, 1 -> rw cube
  | Move.Rw, 2 -> rw2 cube
  | Move.Rw, 3 -> rw3 cube
  | Move.Uw, 1 -> uw cube
  | Move.Uw, 2 -> uw2 cube
  | Move.Uw, 3 -> uw3 cube
  | Move.Bw, 1 -> bw cube
  | Move.Bw, 2 -> bw2 cube
  | Move.Bw, 3 -> bw3 cube
  | Move.Lw, 1 -> lw cube
  | Move.Lw, 2 -> lw2 cube
  | Move.Lw, 3 -> lw3 cube
  | Move.Dw, 1 -> dw cube
  | Move.Dw, 2 -> dw2 cube
  | Move.Dw, 3 -> dw3 cube
  | Move.X, 1 -> x cube
  | Move.X, 2 -> x2 cube
  | Move.X, 3 -> x3 cube
  | Move.Y, 1 -> y cube
  | Move.Y, 2 -> y2 cube
  | Move.Y, 3 -> y3 cube
  | Move.Z, 1 -> z cube
  | Move.Z, 2 -> z2 cube
  | Move.Z, 3 -> z3 cube
  | Move.M, 1 -> m cube
  | Move.M, 2 -> m2 cube
  | Move.M, 3 -> m3 cube
  | Move.E, 1 -> e cube
  | Move.E, 2 -> e2 cube
  | Move.E, 3 -> e3 cube
  | Move.S, 1 -> s cube
  | Move.S, 2 -> s2 cube
  | Move.S, 3 -> s3 cube
  | _ -> ()

