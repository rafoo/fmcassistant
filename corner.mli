type t
val n : int
val orientations : int

val of_int : int -> t
val to_int : t -> int

val corner_pos : t -> int -> (Face.face * int * int)

type cube = (t * int) array

val of_facelet : Facelet.cube -> cube

val to_string : t * int -> string
val cube_to_string : cube -> string
