
let main () =
  Arg.parse
    []
    (fun file ->
      Display.init file;
    ) "Please provide a file name"

let () = main ()
