{

  open Parser

}

rule token = parse
    | [ ' ' '\t' ] { token lexbuf }
    | '\n' { Lexing.new_line lexbuf; token lexbuf }

    | "let" { LET }
    | ":=" { DEF }
    | "in" { IN }

    | 'F' { F }
    | 'R' { R }
    | 'U' { U }
    | 'B' { B }
    | 'L' { L }
    | 'D' { D }

    | '2' { SQ }
    | '\'' { PRIME }

    | 'f' { FW }
    | 'r' { RW }
    | 'u' { UW }
    | 'b' { BW }
    | 'l' { LW }
    | 'd' { DW }

    | 'x' { X }
    | 'y' { Y }
    | 'z' { Z }
    | 'M' { M }
    | 'E' { E }
    | 'S' { S }

    | '.' { DISPLAY }

    | '<' { CUSTOM (string lexbuf) }

    | '#' { comment lexbuf }

    | eof { EOF }

and comment = parse
  | '\n' { Lexing.new_line lexbuf; token lexbuf }
  | _ { comment lexbuf }
  | eof { EOF }

and string = parse
  | '>' { "" }
  | _ as c { String.make 1 c ^ string lexbuf }
  | eof { failwith "Lexing Error: unterminated string" }
