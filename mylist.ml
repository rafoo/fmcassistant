
let rec all_distinct = function
  | [] -> true
  | a :: l -> not (List.mem a l) && all_distinct l

let rec last = function
  | [] -> failwith "last"
  | [a] -> a
  | _ :: l -> last l

let rec butlast = function
  | [] -> failwith "butlast"
  | [a] -> []
  | a :: l -> a :: butlast l

let rec ( @^ ) l n = match n with
  | 0 -> []
  | _ -> l @ (l @^ (n - 1))
