type cube
val init_cube : unit -> cube
val get_face : cube -> Face.face -> int -> int -> Face.face

(* This is a low-level function,
   the value returned represents the color of the given facelet,
   not the face corresponding to this color.

   This function is exported to allow the cube to be displayed. For
   cube solving or analysis, use get_face instead. *)
val get : cube -> Face.face -> int -> int -> int

val apply_move : cube -> (Move.move * int) -> unit

val reinit : cube -> unit
