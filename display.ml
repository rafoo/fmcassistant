open GMain
open GdkKeysyms

let sizex = ref 0
let sizey = ref 0

let _30deg = 3.14592 /. 6.

let scale = 10
let cos30 x = int_of_float (10. *. cos _30deg *. float_of_int x)
let sin30 x = int_of_float (10. *. sin _30deg *. float_of_int x)

let point3d x y z =
  let ox = !sizex / 2 in
  let oy = (!sizey / 2) -25 in
  (ox + cos30 (x + z), oy - (scale * y) - sin30 (z - x))

let fpoly x y =
  let xf = 2 * x - 1 in
  let xf' = 2 * x + 1 in
  let yf = 2 * y - 1 in
  let yf' = 2 * y + 1 in
  let zf = -3 in
  [ point3d xf yf zf;
    point3d xf' yf zf;
    point3d xf' yf' zf;
    point3d xf yf' zf; ]

let dpoly x y =
  let xf = 2 * x - 1 in
  let xf' = 2 * x + 1 in
  let yf = 2 * y - 7 in
  let yf' = 2 * y - 5 in
  let zf = -3 in
  [ point3d xf yf zf;
    point3d xf' yf zf;
    point3d xf' yf' zf;
    point3d xf yf' zf; ]

let rpoly x y =
  let xf = 3 in
  let yf = 2 * y - 1 in
  let yf' = 2 * y + 1 in
  let zf = 2 * x - 1 in
  let zf' = 2 * x + 1 in
  [ point3d xf yf zf;
    point3d xf yf' zf;
    point3d xf yf' zf';
    point3d xf yf zf'; ]

let upoly x y =
  let yf = 3 in
  let xf = 2 * x - 1 in
  let xf' = 2 * x + 1 in
  let zf = 2 * y - 1 in
  let zf' = 2 * y + 1 in
  [ point3d xf yf zf;
    point3d xf' yf zf;
    point3d xf' yf zf';
    point3d xf yf zf'; ]

let lpoly x y =
  let (xm, ym) = point3d (-3) (-3) (-3) in
  let (xo, yo) = (xm - 30, ym - 30) in
  [ (xo + (20 * x) - 10, yo - (20 * y) + 10);
    (xo + (20 * x) - 10, yo - (20 * y) - 10);
    (xo + (20 * x) + 10, yo - (20 * y) - 10);
    (xo + (20 * x) + 10, yo - (20 * y) + 10); ]

let bpoly x y =
  let (xm, ym) = point3d 3 (-3) 3 in
  let (xo, yo) = (xm + 30, ym - 30) in
  [ (xo + (20 * x) - 10, yo - (20 * y) + 10);
    (xo + (20 * x) - 10, yo - (20 * y) - 10);
    (xo + (20 * x) + 10, yo - (20 * y) - 10);
    (xo + (20 * x) + 10, yo - (20 * y) + 10); ]

let color_of_face n = [| `WHITE; `NAME "yellow"; `NAME "green"; `NAME "blue"; `NAME "red"; `NAME "orange" |].(n)

let poly = function
  | Face.F -> fpoly
  | Face.R -> rpoly
  | Face.U -> upoly
  | Face.B -> bpoly
  | Face.L -> lpoly
  | Face.D -> dpoly

let display_cube cube (drawable : GDraw.drawable) =
  let (width, height) = drawable#size in
  drawable#set_background `WHITE;
  drawable#set_foreground `WHITE;
  drawable#rectangle ~x:0 ~y:0 ~width ~height ~filled:true ();
  sizex := width;
  sizey := height;
  List.iter
    (fun f ->
      for x = -1 to 1 do
        for y = -1 to 1 do
          drawable#set_foreground (color_of_face (Facelet.get cube f (x+1) (y+1)));
          drawable#polygon ~filled:true (poly f x y);
          drawable#set_foreground `BLACK;
          drawable#polygon ~filled:false (poly f x y);
        done;
      done)
    (let open Face in [F; R; U; B; L; D ])

type cube = { facelet : Facelet.cube;
              premove : Move.cmd list;
              mutable before : Move.cmd list;
              mutable after : Move.cmd list }

module CornerCycle = Perm.Cycle (Corner)
module EdgeCycle = Perm.Cycle (Edge)

let display_seq cube (ta : GText.view) =
  let corner_cube = Corner.of_facelet cube.facelet in
  let corner_cycles = CornerCycle.perm_to_cycles corner_cube in
  let edge_cube = Edge.of_facelet cube.facelet in
  let edge_cycles = EdgeCycle.perm_to_cycles edge_cube in
  ta#buffer#set_text (
      Corner.cube_to_string corner_cube ^ "\n" ^
      Edge.cube_to_string edge_cube ^ "\n" ^
      CornerCycle.cycles_to_string corner_cycles ^ "\n" ^
      EdgeCycle.cycles_to_string edge_cycles ^ "\n" ^
      Move.seq_to_string cube.before ^ " | " ^
      Move.seq_to_string cube.after ^ "\n\n" ^
      Move.seq_to_string (Move.normalize cube.before) ^ " | " ^
      Move.seq_to_string (Move.normalize cube.after))

let apply_move cube mv =
  cube.before <- cube.before @ [mv];
  assert (mv = List.hd cube.after);
  cube.after <- List.tl cube.after;
  match mv with
  | Move.Mv (move, repeat) ->
       Facelet.apply_move cube.facelet (move, repeat)
  | Move.Custom _ as c ->
     List.iter (function
                | Move.Mv (move, repeat) ->
                   Facelet.apply_move cube.facelet (move, repeat)
                | _ -> ())
               (Move.unfold [c])
  | Move.Disp -> ()

let rev_apply_move cube mv =
  cube.after <- mv :: cube.after;
  assert (mv = Mylist.last cube.before);
  cube.before <- Mylist.butlast cube.before;
  match mv with
  | Move.Mv (move, repeat) ->
       Facelet.apply_move cube.facelet (move, 4 - repeat)
  | Move.Custom (x, b) ->
     List.iter (function
                | Move.Mv (move, repeat) ->
                   Facelet.apply_move cube.facelet (move, repeat)
                | _ -> ())
               (Move.unfold [Move.Custom (x, not b)])
  | Move.Disp -> ()

class cube_widget ?width ?height ?packing ?show ?textview ~cube () =
  let vbox = GPack.vbox ?width ?height ?packing ?show () in
  let da = GMisc.drawing_area ~packing:(vbox#pack ~expand:true) () in
  let insertbox = GPack.hbox ~packing:vbox#pack () in
  let insertview = GText.view ~packing:insertbox#add () in
  let hbox = GPack.hbox ~packing:vbox#pack () in
  let parse s = Parse_sequence.parse (Lexing.from_string s) in
  object (self)
    inherit GObj.widget vbox#as_widget
    val insertbutton = GButton.button ~label:"Insert" ~packing:insertbox#pack ()
    val firstbutton = GButton.button ~label:"|◀" ~packing:hbox#add ()
    val backstepbutton = GButton.button ~label:"◀◀" ~packing:hbox#add ()
    val previousbutton = GButton.button ~label:"◀" ~packing:hbox#add ()
    val nextbutton = GButton.button ~label:"▶" ~packing:hbox#add ()
    val stepbutton = GButton.button ~label:"▶▶" ~packing:hbox#add ()
    val lastbutton = GButton.button ~label:"▶|" ~packing:hbox#add ()
    val drawable = lazy (new GDraw.drawable da#misc#window)

    method insertbutton = insertbutton
    method firstbutton = firstbutton
    method previousbutton = previousbutton
    method nextbutton = nextbutton
    method stepbutton = stepbutton
    method backstepbutton = backstepbutton
    method lastbutton = lastbutton

    method disp =
      begin
        match textview with
        | None -> ()
        | Some tv -> display_seq cube tv
      end;
      previousbutton#misc#set_sensitive (cube.before != []);
      backstepbutton#misc#set_sensitive (cube.before != []);
      firstbutton#misc#set_sensitive (cube.before != []);
      nextbutton#misc#set_sensitive (cube.after != []);
      stepbutton#misc#set_sensitive (cube.after != []);
      lastbutton#misc#set_sensitive (cube.after != []);
      display_cube cube.facelet (Lazy.force drawable)
    method get_text =
      let s = parse (insertview#buffer#get_text ()) in
      insertview#buffer#set_text "";
      s
    method insert_after s =
      cube.after <- s @ cube.after;
      self#disp
    method insert_before s =
      cube.after <- s @ cube.after;
      List.iter (apply_move cube) s;
      self#disp
    method next =
      apply_move cube (List.hd cube.after);
      self#disp
    method prev =
      rev_apply_move cube (Mylist.last cube.before);
      self#disp
    method step =
      while cube.after != [] && List.hd cube.after != Move.Disp do
        apply_move cube (List.hd cube.after)
      done;
      if cube.after != [] then apply_move cube Move.Disp;
      self#disp
    method backstep =
      while cube.before != [] && Mylist.last cube.before != Move.Disp do
        rev_apply_move cube (Mylist.last cube.before)
      done;
      if cube.before != [] then rev_apply_move cube Move.Disp;
      self#disp
    method first =
      List.iter (rev_apply_move cube) (List.rev cube.before)
    method last =
      List.iter (apply_move cube) cube.after
  end


let init file =

  let cube_stack = ref [] in

  let refresh () =
    Printf.eprintf "Opening file %s\n" file;
    flush stderr;
    let s = Parse_sequence.parse (Lexing.from_channel (open_in file)) in
    let si = Move.inverse_seq s in
    List.iter (fun (cube, widget, b) ->
        Facelet.reinit cube.facelet;
        cube.after <- cube.premove;
        List.iter (apply_move cube) cube.premove;
        cube.before <- [];
        cube.after <- if b then si else s;
        if b then List.iter (apply_move cube) si) !cube_stack
  in

  (* Borrowed from https://ocaml.org/learn/tutorials/introduction_to_gtk.html *)
  let _ = GtkMain.Main.init () in

  let window = GWindow.window ~title:"FMC assistant" () in
  ignore (window#connect#destroy ~callback:Main.quit);
  let vbox = GPack.vbox ~packing:window#add () in
  let tophbox = GPack.hbox ~packing:vbox#pack () in
  let refreshbutton = GButton.button ~label:"Refresh" ~packing:tophbox#pack () in
  let quitbutton = GButton.button ~label:"Quit" ~packing:tophbox#pack () in
  let hbox = GPack.hbox ~packing:vbox#add () in

  let next () = List.iter (fun (_, w, b) -> if b then w#prev else w#next) !cube_stack in
  let prev () = List.iter (fun (_, w, b) -> if b then w#next else w#prev) !cube_stack in
  let first () = List.iter (fun (_, w, b) -> if b then w#last else w#first) !cube_stack in
  let last () = List.iter (fun (_, w, b) -> if b then w#first else w#last) !cube_stack in
  let step () = List.iter (fun (_, w, b) -> if b then w#backstep else w#step) !cube_stack in
  let backstep () = List.iter (fun (_, w, b) -> if b then w#step else w#backstep) !cube_stack in
  let insert w () =
    let s = w#get_text in
    let si = Move.inverse_seq s in
    List.iter (fun (_, w, b) -> if b then w#insert_after si else w#insert_before s) !cube_stack
  in
  let refresh_action () =
    refresh ();
    List.iter (fun (_, w, _) -> w#disp) !cube_stack
  in

  let new_cube_widget ?textview ?(premove=[]) () =
    let cube = {facelet = Facelet.init_cube ();
                premove = premove;
                before = [];
                after = []}
    in
    let w = new cube_widget ~packing:hbox#add ?textview ~cube () in
    cube_stack := (cube, w, false) :: !cube_stack;
    ignore (w#insertbutton#connect#clicked ~callback: (insert w));
    ignore (w#firstbutton#connect#clicked ~callback: first);
    ignore (w#backstepbutton#connect#clicked ~callback: backstep);
    ignore (w#previousbutton#connect#clicked ~callback: prev);
    ignore (w#nextbutton#connect#clicked ~callback: next);
    ignore (w#stepbutton#connect#clicked ~callback: step);
    ignore (w#lastbutton#connect#clicked ~callback: last)
  in
  let new_reversed_cube ?(premove=[]) () =
    let cube = {facelet = Facelet.init_cube ();
                premove = premove;
                before = [];
                after = []}
    in
    let w = new cube_widget ~packing:hbox#add ~cube () in
    cube_stack := (cube, w, true) :: !cube_stack;
    ignore (w#insertbutton#connect#clicked ~callback: (insert w));
    ignore (w#firstbutton#connect#clicked ~callback: last);
    ignore (w#backstepbutton#connect#clicked ~callback: step);
    ignore (w#previousbutton#connect#clicked ~callback: next);
    ignore (w#nextbutton#connect#clicked ~callback: prev);
    ignore (w#stepbutton#connect#clicked ~callback: backstep);
    ignore (w#lastbutton#connect#clicked ~callback: first)
  in

  ignore (quitbutton#connect#clicked ~callback: Main.quit);
  ignore (refreshbutton#connect#clicked ~callback: refresh_action);

  ignore (window#event#connect#expose ~callback:(fun _ -> List.iter (fun (_, w, _) -> w#disp) !cube_stack; false));


  let textview = GText.view ~packing:vbox#pack () in
  new_cube_widget ~textview ();
  new_reversed_cube ();
  refresh ();

  (* Display the windows and enter Gtk+ main loop *)
  window#show ();
  Main.main ()
