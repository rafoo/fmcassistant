type face = U | D | F | B | R | L

val to_int : face -> int
val of_int : int -> face
val to_string : face -> string

