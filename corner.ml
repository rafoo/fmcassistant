open Face

type t = URF | UFL | ULB | UBR | DFR | DLF | DBL | DRB
let n = 8
let orientations = 3
let arr = [| URF; UFL; ULB; UBR; DFR; DLF; DBL; DRB |]
let of_int n = arr.(n)

let to_int = function
  | URF -> 0
  | UFL -> 1
  | ULB -> 2
  | UBR -> 3
  | DFR -> 4
  | DLF -> 5
  | DBL -> 6
  | DRB -> 7

(* for i = 0 to 7 do assert (to_int (of_int i) = i) done;; *)

type cycle = t list        (* length >= 3, first = last, all other distinct *)
and permutation = cycle list    (* disjoint support *)

type cube = (t * int) array

(* Gives the positions of the three stickers of a t *)
let corner_pos c o =
  let arr = [|
   [| (U, 2, 0); (R, 0, 2); (F, 2, 2) |] ;
   [| (U, 0, 0); (F, 0, 2); (L, 2, 2) |] ;
   [| (U, 0, 2); (L, 0, 2); (B, 2, 2) |] ;
   [| (U, 2, 2); (B, 0, 2); (R, 2, 2) |] ;
   [| (D, 2, 2); (F, 2, 0); (R, 0, 0) |] ;
   [| (D, 0, 2); (L, 2, 0); (F, 0, 0) |] ;
   [| (D, 0, 0); (B, 2, 0); (L, 0, 0) |] ;
   [| (D, 2, 0); (R, 2, 0); (B, 0, 0) |] |] in
  arr.(to_int c).(o mod 3)

let oriented_corner_from_faces f1 f2 =
  match (f1, f2) with
  | (U, R) -> (URF, 0) | (R, F) -> (URF, 1) | (F, U) -> (URF, 2)
  | (U, F) -> (UFL, 0) | (F, L) -> (UFL, 1) | (L, U) -> (UFL, 2)
  | (U, L) -> (ULB, 0) | (L, B) -> (ULB, 1) | (B, U) -> (ULB, 2)
  | (U, B) -> (UBR, 0) | (B, R) -> (UBR, 1) | (R, U) -> (UBR, 2)
  | (D, F) -> (DFR, 0) | (F, R) -> (DFR, 1) | (R, D) -> (DFR, 2)
  | (D, L) -> (DLF, 0) | (L, F) -> (DLF, 1) | (F, D) -> (DLF, 2)
  | (D, B) -> (DBL, 0) | (B, L) -> (DBL, 1) | (L, D) -> (DBL, 2)
  | (D, R) -> (DRB, 0) | (R, B) -> (DRB, 1) | (B, D) -> (DRB, 2)
  | _ -> failwith "oriented_corner_from_faces"

let get_corner cube c =
  let (f1, x1, y1) = corner_pos c 0 in
  let (f2, x2, y2) = corner_pos c 1 in
  let col1 = Facelet.get_face cube f1 x1 y1 in
  let col2 = Facelet.get_face cube f2 x2 y2 in
  oriented_corner_from_faces col1 col2

let of_facelet cube = Array.map (get_corner cube) arr

let to_string (c, o) =
  let ((f1, _, _), (f2, _, _), (f3, _, _)) = (corner_pos c o, corner_pos c (o+1), corner_pos c (o+2)) in
  Face.to_string f1 ^ Face.to_string f2 ^ Face.to_string f3

let cube_to_string cube =
  let a = ref "[" in
  Array.iteri
    (fun i c ->
      a := !a ^
        Printf.sprintf "%s -> %s, "
           (to_string (arr.(i), 0))
           (to_string c))
    cube;
  !a ^ "]"
